package ch.zhaw.psit.keeptrack.conf;

import ch.zhaw.psit.keeptrack.Loggers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.fail;

public class ConfigLoaderTest {

    private ConfigLoader configLoader;

    @BeforeEach
    public void setUp() throws Exception {
        configLoader = ConfigLoader.getInstance();
        configLoader.setSource("test-config.config.json");
    }

    @Test
    public void getInstance() {
        ConfigLoader configLoader = ConfigLoader.getInstance();
        Assertions.assertNotNull(configLoader);
    }

    @Test
    public void getInstanceWithInvalidDefaultSource() throws NoSuchFieldException, IllegalAccessException {
        // set invalid default source
        String trueSource = System.setProperty("KT_CONF_SOURCE", "someInvalidSourceString");

        // reset ConfigLoader using reflection api
        Field instanceField = ConfigLoader.class.getDeclaredField("instance");
        instanceField.setAccessible(true);
        instanceField.set(null, null);

        // config loader instance is reset
        // and should now create new instance,
        // fail and throw a runtime exception.
        try {
            ConfigLoader.getInstance();
            Assertions.fail();
        } catch (RuntimeException ex) {
            Loggers.DEVELOPMENT_LOGGER.info("ConfigLoader behaves correctly on invalid config.");
        }

        // re-set valid default source
        // since instance should still be null we don't have to reset it
        if (trueSource != null) {
            System.setProperty("KT_CONF_SOURCE", trueSource);
        } else System.clearProperty("KT_CONF_SOURCE");

        // run getInstance test again just to be sure changes are flushed.
        getInstance();
    }

    @Test
    public void setSource() {
        configLoader = ConfigLoader.getInstance();
        try {
            configLoader.setSource("test-config.config.json");
        } catch (ConfigurationException e) {
            fail("Can't find config.");
        }
    }

    @Test
    public void setInvalidSource() {
        configLoader = ConfigLoader.getInstance();
        try {
            configLoader.setSource("some-invalid-config");
            fail("Didn't error.");
        } catch (ConfigurationException e) {
            // pass, errored as expected
        }
    }

    @Test
    public void setMalformedSource() {
        configLoader = ConfigLoader.getInstance();
        try {
            configLoader.setSource("malformed-config");
            fail("Didn't error.");
        } catch (ConfigurationException e) {
            // pass, errored as expected
        }
    }

    @Test
    public void get() {
        Assertions.assertEquals("success", configLoader.get("test"));
    }

    @Test
    public void getProperty() {
        System.setProperty("test", "success_property");
        Assertions.assertEquals("success_property", configLoader.get("test"));
    }

    @AfterAll
    public static void tearDown() throws ConfigurationException {
        ConfigLoader.getInstance().setSource("test.config.json");
    }
}