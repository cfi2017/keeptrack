package ch.zhaw.psit.keeptrack.e2e;

import ch.zhaw.psit.keeptrack.ChromeBrowser;
import ch.zhaw.psit.keeptrack.EmbeddedTomcatServer;
import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.conf.ConfigLoader;
import ch.zhaw.psit.keeptrack.dao.ConnectionFactory;
import ch.zhaw.psit.keeptrack.dao.UserDAO;
import ch.zhaw.psit.keeptrack.exception.UserExistsException;
import ch.zhaw.psit.keeptrack.model.User;
import org.apache.catalina.Server;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;

@EnabledIfSystemProperty(named = "END_TO_END", matches = "true")
public class LoginTest {

    private static WebDriver browser;
    private static Server server;

    /**
     * getServer browser
     */
    @BeforeAll
    static void setUp() throws Exception {
        server = EmbeddedTomcatServer.getServer();
        browser = ChromeBrowser.getBrowser();
    }

    /**
     * reset database before each test
     * reload login page before each test, giving a fresh experience
     */
    @BeforeEach
    void beforeEach() {
        try {
            String path = System.getProperty("user.dir") + "/sql/createInMemDB.sql";
            RunScript.execute(ConnectionFactory.getInstance().createConnection(), new FileReader(path));
        } catch (SQLException | FileNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error recreating SQL database: ", ex);
        }

        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/login");
    }

    /**
     * stop browser
     */
    @AfterAll
    static void tearDown() throws Exception {
    }

    @Test
    void loginPageLoads() {
        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/login");
        WebElement title = browser.findElement(By.cssSelector("h3"));
        assertTrue((title.isDisplayed()));
    }

    @Test
    void canFillUsername() {
        this.canFillField("username", "test");
    }

    @Test
    void canFillPassword() {
        this.canFillField("password", "test");
    }

    @Test
    void isUsernameRequired() {
        this.isRequired("username");
    }

    @Test
    void isPasswordRequired() {
        this.isRequired("password");
    }

    /*
    @Test
    void canLogin() throws Exception {

        RegisterTest.setUp();
        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/register");

        RegisterTest registerTest = new RegisterTest();
        registerTest.canRegisterUser();

        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/login");
        fillLoginFields("testusername");

        WebElement submitButton = browser.findElement(By.id("LoginButton"));
        submitButton.click();
        // submitButton.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(browser, 5);
        wait.until(ExpectedConditions.urlContains("dashboard"));


        WebElement success = browser.findElement(By.id("loggedIn"));
        Assertions.assertNotNull(success);
        Assertions.assertTrue(success.isDisplayed());

    }*/

    private void fillLoginFields(String username) {
        WebElement usernameInput = browser.findElement(By.id("username"));
        WebElement passwordInput = browser.findElement(By.id("password"));

        usernameInput.sendKeys(username);
        passwordInput.sendKeys("test");
    }

    private void canFillField(String id, String text) {
        WebElement field = browser.findElement(By.id(id));
        field.sendKeys(text);
        Assertions.assertEquals(text, field.getAttribute("value"));
        String errorId = "error" + (("" + id.charAt(0)).toUpperCase()) + id.substring(1);
        Assertions.assertFalse(browser.findElement(By.id(errorId)).isDisplayed());
    }

    private void isRequired(String id) {
        WebElement field = browser.findElement(By.id(id));
        field.clear();
        browser.findElement(By.id("LoginButton")).click();
        String errorId = "error" + (("" + id.charAt(0)).toUpperCase()) + id.substring(1);
        Assertions.assertTrue(browser.findElement(By.id(errorId)).isDisplayed());
    }
}
