package ch.zhaw.psit.keeptrack;

import ch.zhaw.psit.keeptrack.conf.ConfigLoader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeBrowser {

    private static WebDriver browser = null;

    public static WebDriver getBrowser() {
        if (browser != null) return browser;
        if (ConfigLoader.getInstance().get("CHROME_DRIVER") != null) {
            System.setProperty("webdriver.chrome.driver", ConfigLoader.getInstance().get("CHROME_DRIVER"));
        }
        ChromeOptions options = new ChromeOptions();
        // launch chrome in headless mode
        if (ConfigLoader.getInstance().get("HEADLESS") != null) {
            Loggers.DEVELOPMENT_LOGGER.info("Starting Chrome Headless");
            options.addArguments("--headless");
        }
        browser = new ChromeDriver(options);
        return browser;
    }
}
