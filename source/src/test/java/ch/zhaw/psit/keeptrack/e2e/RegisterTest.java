package ch.zhaw.psit.keeptrack.e2e;

import ch.zhaw.psit.keeptrack.ChromeBrowser;
import ch.zhaw.psit.keeptrack.EmbeddedTomcatServer;
import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.conf.ConfigLoader;
import ch.zhaw.psit.keeptrack.dao.ConnectionFactory;
import org.apache.catalina.Server;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

@EnabledIfSystemProperty(named = "END_TO_END", matches = "true")
public class RegisterTest {

    private static WebDriver browser;
    private static Server server;

    /**
     * getServer browser
     */
    @BeforeAll
    static void setUp() throws Exception {
        server = EmbeddedTomcatServer.getServer();
        browser = ChromeBrowser.getBrowser();
    }

    /**
     * reset database before each test
     * reload registration page before each test, giving a fresh experience
     */
    @BeforeEach
    void beforeEach() {
        try {
            String path = System.getProperty("user.dir") + "/sql/createInMemDB.sql";
            RunScript.execute(ConnectionFactory.getInstance().createConnection(), new FileReader(path));
        } catch (SQLException | FileNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error recreating SQL database: ", ex);
        }
        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/register");
    }

    /**
     * stop browser
     */
    @AfterAll
    static void tearDown() throws Exception {
    }

    @Test
    void registerPageLoads() {
        browser.get(ConfigLoader.getInstance().get("BASE_URL") + "/register");
        WebElement title = browser.findElement(By.cssSelector("h2"));
        assertTrue((title.isDisplayed()));
    }

    @Test
    void canFillUsername() {
        this.canFillField("username", "test");
    }

    @Test
    void canFillName() {
        this.canFillField("name", "test");
    }

    @Test
    void canFillSurname() {
        this.canFillField("surname", "test");
    }

    @Test
    void canFillEmail() {
        this.canFillField("email", "test@test.ch");
    }

    @Test
    void canFillAddress() {
        this.canFillField("address", "test");
    }

    @Test
    void canFillPassword() {
        this.canFillField("password", "test");
    }
    @Test
    void canFillConfirmPassword() {
        this.canFillField("confirmPassword", "test");
    }

    @Test
    void isUsernameRequired() {
        this.isRequired("username");
    }

    @Test
    void isNameRequired() {
        this.isRequired("name");
    }

    @Test
    void isSurnameRequired() {
        this.isRequired("surname");
    }

    @Test
    void isEmailRequired() {
        this.isRequired("email");
    }

    @Test
    void isAddressRequired() {
        this.isRequired("address");
    }

    @Test
    void isPasswordRequired() {
        this.isRequired("password");
    }

    @Test
    void isConfirmPasswordRequired() {
        this.isRequired("confirmPassword");
    }

    @Test
    void usernameIsLimitedTo20Characters() {
        charLimitIs("username", 20);
    }

    @Test
    void nameInputIsLimitedTo30Characters() {
        charLimitIs("name", 30);
    }

    @Test
    void surnameIsLimitedTo30Characters() {
        charLimitIs("surname", 30);
    }

    @Test
    void canRegisterUser() {
        fillRegistrationFields("testusername", "testemail@test.ch");

        WebElement submitButton = browser.findElement(By.id("RegisterButton"));
        submitButton.click();
        // submitButton.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(browser, 5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("success")));


        WebElement success = browser.findElement(By.id("success"));
        Assertions.assertNotNull(success);
        Assertions.assertTrue(success.isDisplayed());

    }

    private void fillRegistrationFields(String username, String email) {
        WebElement usernameInput = browser.findElement(By.id("username"));
        WebElement nameInput = browser.findElement(By.id("name"));
        WebElement surnameInput = browser.findElement(By.id("surname"));
        WebElement addressInput = browser.findElement(By.id("address"));
        WebElement emailInput = browser.findElement(By.id("email"));
        WebElement passwordInput = browser.findElement(By.id("password"));
        WebElement confirmPasswordInput = browser.findElements(By.cssSelector("input[type=\"password\"]")).get(1);

        usernameInput.sendKeys(username);
        nameInput.sendKeys("testname");
        surnameInput.sendKeys("testsurname");
        addressInput.sendKeys("test");
        emailInput.sendKeys(email);
        passwordInput.sendKeys("test");
        confirmPasswordInput.sendKeys("test");
    }

    private void canFillField(String id, String text) {
        WebElement field = browser.findElement(By.id(id));
        field.sendKeys(text);
        Assertions.assertEquals(text, field.getAttribute("value"));
        String errorId = "error" + (("" + id.charAt(0)).toUpperCase()) + id.substring(1);
        Assertions.assertFalse(browser.findElement(By.id(errorId)).isDisplayed());
    }

    private void isRequired(String id) {
        WebElement field = browser.findElement(By.id(id));
        field.clear();
        browser.findElement(By.id("RegisterButton")).click();
        String errorId = "error" + (("" + id.charAt(0)).toUpperCase()) + id.substring(1);
        Assertions.assertTrue(browser.findElement(By.id(errorId)).isDisplayed());
    }

    private void charLimitIs(String id, int limit) {
        WebElement field = browser.findElement(By.id(id));
        for (Integer i = 0; i < limit; i++) {
            field.sendKeys("t");
        }
        field.sendKeys("t");
        Assertions.assertEquals(limit, field.getAttribute("value").length());
    }

}
