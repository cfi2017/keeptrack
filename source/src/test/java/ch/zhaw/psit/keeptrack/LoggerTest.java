package ch.zhaw.psit.keeptrack;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LoggerTest {

    @Test
    public void testLogger() {
        Assertions.assertEquals("DEVELOPMENT_LOG", Loggers.DEVELOPMENT_LOGGER.getName());
    }

}
