package ch.zhaw.psit.keeptrack.util;

import ch.zhaw.psit.keeptrack.Loggers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CredentialsTest {

    @Test
    public void hash() {

        String result = Credentials.getInstance().hash("test");
        Assertions.assertEquals("9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08".toLowerCase(), result);

    }

    @Test
    public void testInvalidAlgorithm() {
        Credentials.getInstance().setAlgorithm("");
        try {
            String result = Credentials.getInstance().hash("test");
        } catch (RuntimeException ex) {
            Loggers.DEVELOPMENT_LOGGER.info("Runtime exception caught. Test successful.");
        }
        Credentials.getInstance().setAlgorithm("SHA-256");
    }
}