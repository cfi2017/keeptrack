package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.exception.UserExistsException;
import ch.zhaw.psit.keeptrack.model.User;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;


public class UserDAOTest {

    private UserDAO dao = new UserDAO();
    private User user = new User();

    @BeforeEach
    public void setUp() {
        try {
            String path = System.getProperty("user.dir") + "/sql/createInMemDB.sql";
            RunScript.execute(ConnectionFactory.getInstance().createConnection(), new FileReader(path));
        } catch (SQLException | FileNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error recreating SQL database: ", ex);
        }
        user = new User();
    }

    @Test
    public void create() {
        try {
            user.setUsername("user-create");
            user.setAddress("");
            user.setEmail("user-create@test.ch");
            user.setPassword("");
            user.setSurname("");
            user.setName("");
            user.setBirthday(Date.valueOf(LocalDate.now()));
            dao.create(user);
            Assertions.assertNotNull(user.getId());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.warn("Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void update() {
        create();
        user.setUsername("user-update");
        try {
            user = dao.update(user);
            Assertions.assertEquals("user-update", user.getUsername());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error in SQL statement", ex);
            Assertions.fail();
        }

    }

    @Test
    public void delete() {
        create();
        try {
            dao.delete(user);
            Assertions.assertFalse(dao.retrieve(user.getId()).isPresent());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Unknown Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void retrieve() {
        create();
        try {
            Integer id = user.getId();
            user = null;
            Optional<User> result = dao.retrieve(id);
            Assertions.assertTrue(result.isPresent());
            user = result.get();
            Assertions.assertEquals(id, user.getId());
            Assertions.assertEquals("user-create", user.getUsername());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Unknown Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void register() {
        user.setUsername("register-username");
        user.setPassword("test");
        user.setBirthday(Date.valueOf(LocalDate.now()));
        user.setName("register");
        user.setSurname("register");
        user.setEmail("register-email@test.ch");
        user.setAddress("");

        try {
            user = dao.register(user);
            Assertions.assertEquals("register-username", user.getUsername());
            Assertions.assertNotNull(user.getId());
        } catch (UserExistsException | SQLException ex) {
            Assertions.fail();
        }
    }

    @Test
    public void registerExistingUserEmail() {
        register();

        try {
            user.setUsername("register-existing-user-email");
            user.setId(null);
            user = dao.register(user);
            Assertions.fail("User doesn't exist although they should.");
        } catch (UserExistsException ex) {
            Assertions.assertNotNull(ex.getUser());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Unknown error: ", ex);
            Assertions.fail();
        }

    }

    @Test
    public void registerExistingUserUsername() {
        register();

        try {
            user.setEmail("register-existing-user-username");
            user.setId(null);
            user = dao.register(user);
            Assertions.fail("User doesn't exist although they should.");
        } catch (UserExistsException ex) {
            Assertions.assertNotNull(ex.getUser());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Unknown error: ", ex);
            Assertions.fail();
        }

    }
}

