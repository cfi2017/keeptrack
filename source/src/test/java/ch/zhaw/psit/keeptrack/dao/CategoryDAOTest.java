package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.model.Category;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;


public class CategoryDAOTest {

    private final CategoryDAO dao = new CategoryDAO();
    private Category category = new Category();

    @BeforeEach
    public void setUp() {
        try {
            String path = System.getProperty("user.dir") + "/sql/createInMemDB.sql";
            RunScript.execute(ConnectionFactory.getInstance().createConnection(), new FileReader(path));
        } catch (SQLException | FileNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error recreating SQL database: ", ex);
        }
        category = new Category();
    }

    @Test
    public void create() {
        try {
            category.setName("test");
            category = dao.create(category);
            Assertions.assertNotNull(category.getId());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.warn("Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void update() throws SQLException {

        Category category = new Category();
        category.setName("test");
        category = dao.create(category);
        category.setName("test2");
        dao.update(category);

        Category c = dao.retrieve(category.getId()).get();
        Assertions.assertEquals("test2", c.getName());

    }

    @Test
    public void delete() {
        try {
            category.setName("test-category-delete");
            category = dao.create(category);
            Loggers.DEVELOPMENT_LOGGER.info("Category id: <{}>", category.getId());

            dao.delete(category);

            Assertions.assertFalse(dao.retrieve(category.getId()).isPresent());

        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.warn("Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void retrieve() throws SQLException {
        create();

        Category c = dao.retrieve(category.getId()).get();
        Assertions.assertEquals("test", c.getName());
    }
}
