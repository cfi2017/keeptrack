package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.model.Category;
import ch.zhaw.psit.keeptrack.model.Item;
import ch.zhaw.psit.keeptrack.model.User;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;

public class ItemDAOTest {

    private final ItemDAO dao = new ItemDAO();
    private Item item = new Item();

    @BeforeEach
    public void setUp() {
        try {
            String path = System.getProperty("user.dir") + "/sql/createInMemDB.sql";
            RunScript.execute(ConnectionFactory.getInstance().createConnection(), new FileReader(path));
        } catch (SQLException | FileNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error recreating SQL database: ", ex);
        }
        item = new Item();
    }

    @Test
    public void create() throws SQLException {

        Category category = new Category();
        category.setName("testCategory");
        category = new CategoryDAO().create(category);
        item.setCategory(category.getId());

        User user = new User();
        user.setPassword("test");
        user.setUsername("test-item-create");
        user.setEmail("test-item-create@test.ch");
        user.setName("test-item-create");
        user = new UserDAO().create(user);

        item.setOwner(user.getId());
        item.setPossessor(user.getId());
        item.setName("test-item-create");
        item.setDescription("test");
        item.setManufacturer("test");
        item.setCondition("good");

        try {
            dao.create(item);
            Assertions.assertNotNull(item.getId());
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.warn("Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void update() throws SQLException {

        // create item
        create();

        // update item
        item.setName("test-item-update");
        dao.update(item);

        Item i = dao.retrieve(item.getId()).get();
        Assertions.assertEquals("test-item-update", i.getName());

    }

    @Test
    public void delete() {
        try {
            create();
            Loggers.DEVELOPMENT_LOGGER.info("Item id: <{}>", item.getId());

            dao.delete(item);

            Assertions.assertFalse(dao.retrieve(item.getId()).isPresent());

        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.warn("Error: ", ex);
            Assertions.fail();
        }
    }

    @Test
    public void retrieve() throws SQLException {
        create();
        Loggers.DEVELOPMENT_LOGGER.info("Item id: <{}>", item.getId());

        Item i = dao.retrieve(item.getId()).get();
        Assertions.assertEquals("test-item-create", i.getName());
    }
}
