package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.conf.ConfigLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionFactoryTest {

    @Test
    void createConnection() throws SQLException {

        Connection connection = ConnectionFactory.getInstance().createConnection();
        Assertions.assertFalse(connection.isClosed());

    }

    @Test
    void createConnectionWithUnknownDBDriver() throws SQLException {

        String trueDBString = System.setProperty("DB_STRING", "jdbc:someInvalidDBType");

        try {
            Connection connection = ConnectionFactory.getInstance().createConnection();
            fail();
        } catch (NotImplementedException ex) {
            // connection factory throws runtime exception as expected
        }

        if (trueDBString != null) {
            System.setProperty("DB_STRING", trueDBString);
        } else System.clearProperty("DB_STRING");

    }
}