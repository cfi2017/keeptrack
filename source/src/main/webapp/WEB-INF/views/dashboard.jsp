<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Dashboard</title>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon" />
    <link href="../../css/foundation.css" rel="stylesheet">
    <link href="../../css/app.css" rel="stylesheet" >
      <link href="../../css/dashboard.css" rel="stylesheet"/>
      <link href="../../css/header.css" rel="stylesheet"/>
</head>

<body>
<% if(session.getAttribute("user")!= null) { %>
    <div id="WhiteBackground" class="grid-container">
        <div class="dashboard grid-x align-center text-center">
            <main class="small-10 cell">

                <div class="grid-x align-justify">
                    <div class=" small-12 medium-4">
                        <a href="add" class="button expanded dashboardbuttons">Gegenstand erfassen</a>
                    </div>
                    <div class=" small-12 medium-4">
                        <a href="view" class="button expanded dashboardbuttons">Eigene Gegenstände</a>
                    </div>
                </div>
                <div class="container" >
                    <div class="grid-x align-center">
                        <div class="small-6 cell large-4">
                            <img src="../../img/keeptrack.png" alt="KeepTrack!">
                        </div>
                    </div>
                </div>
                <div class="grid-x  align-justify">
                    <div class=" small-12 medium-4">
                        <a href="search" class="button expanded dashboardbuttons">Suche</a>
                    </div>
                    <div class=" small-12 medium-4">
                        <a href="profile" class="button expanded dashboardbuttons" >Profil bearbeiten</a>
                    </div>
                </div>
            </main>
        </div>
    </div>

    <script src="../../js/vendor/jquery.js"></script>
    <script src="../../js/vendor/what-input.js"></script>
    <script src="../../js/vendor/foundation.js"></script>
    <script src="../../js/app.js"></script>

<% } else { response.sendRedirect("login"); } %>
</body>
</html>