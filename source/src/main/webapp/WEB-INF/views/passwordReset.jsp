<%@ page import="ch.zhaw.psit.keeptrack.model.User" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.UserDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.util.Credentials" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="ch.zhaw.psit.keeptrack.Loggers" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PasswordReset</title>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon"/>
    <link href="css/foundation.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <link href="css/header.css" rel="stylesheet"/>
</head>
<body>
<% if(session.getAttribute("user")!= null) { %>
    <div id="WhiteBackground" class="grid-container fluid">
        <div class="grid-x align-center">
            <header>
                <div class="container" >
                    <div class="grid-x align-center">
                        <div class="small-10 cell">
                            <h1><img src="../../img/keeptrack.png" alt="Logo as Titel" /></h1>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <main>
            <div class="grid-x text-left" >
                <div class="small-3 cell small-offset-2">
                    <h3>Paswort ändern:</h3>
                </div>
            </div>

            <form data-abide novalidate method="post" action="/passwordReset">
                <div class="grid-x text-left" >
                    <div class="small-6 cell small-offset-3">
                        <div data-abide-error class="alert callout" style="display: none;">
                            <p><i class="fi-alert"></i> Das Passwort stimmt nicht überein.</p>
                        </div>
                        <div class="grid-x align-center">
                            <div class="cell small-10">
                                <label>Altes Passwort
                                    <input id="oldpassword" name="oldpassword" type="password" placeholder="********" required>
                                </label>
                            </div>
                        </div>
                        <div class="grid-x align-center">
                            <div class="cell small-10">
                                <label>Neues Passwort
                                    <input id="newpassword" name="newpassword" type="password" placeholder="********" required>
                                </label>
                            </div>
                        </div>
                        <div class="grid-x align-center">
                            <div class="cell small-10">
                                <label>Bestätigung Passwort
                                    <input type="password" placeholder="********" aria-errormessage="errorPasswordCheck" required
                                           data-equalto="newpassword">
                                    <span class="form-error" id="errorPasswordCheck">
                                        Das Passwort stimmt nicht überein.
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="grid-x text-center" >
                            <div class="small-3 cell small-offset-2 medium-offset-3">
                                <button id="PasswordReset" class="button" type="submit" value="Submit">Passwort ändern</button>
                            </div>
                            <div class="small-2 cell">
                                <a class="button" href="profile">Zurück</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <% if ("POST".equals(request.getMethod())) {
            String oldPassword = Credentials.getInstance().hash(request.getParameter("oldpassword"));
            String newPassword = Credentials.getInstance().hash(request.getParameter("newpassword"));
            User userSession = (User)session.getAttribute("user");
            boolean changePassword = false;

            if(oldPassword.equals(userSession.getPassword())){
                userSession.setPassword(newPassword);
                UserDAO dao = new UserDAO();
                try{
                    dao.update(userSession);
                    changePassword = true;
                }catch(SQLException ex) {
                    Loggers.DEVELOPMENT_LOGGER.warn("Unknown Error: ", ex);
                }

                }else {
                    changePassword = false;
                %>
                    <div class="grid-x text-center">
                        <div class="small-8 cell small-offset-2">
                            <h4> <span id="wrongPassword" style="color: red; text-align: center;" >Altes Passwort stimmt nicht</span></h4>
                        </div>
                    </div>

        <%
            }
        if(changePassword || userSession.getPassword().equals(newPassword)){
        %>
        <div class="grid-x text-center">
            <div class="small-8 cell small-offset-2">
                <h4> <span id="success" style="font-size: medium; color: red; text-align: center;">Passwort wurde erfolgreich geändert</span></h4>
                <br />
            </div>
        </div>
        <%
            } else {
        %>
        <div class="grid-x text-center">
            <div class="small-8 cell small-offset-2">
                <h4><span id="SqlException" style="font-size: medium; color: red; text-align: center;">Die Passwortänderung ist fehlgeschlagen</span></h4>
                <br />
            </div>
        </div>
        <%
            }
        }
        %>
    </main>
</div>
    <script src="../../js/vendor/jquery.js"></script>
    <script src="../../js/vendor/what-input.js"></script>
    <script src="../../js/vendor/foundation.js"></script>
    <script src="../../js/app.js"></script>

<% } else { response.sendRedirect("login"); } %>
</body>
</html>
