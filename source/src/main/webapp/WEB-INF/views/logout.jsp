<%
        response.setContentType("text/html");
        //invalidate the session if exists
        if(session != null) {
            session.invalidate();
            response.sendRedirect("login");
        }
%>