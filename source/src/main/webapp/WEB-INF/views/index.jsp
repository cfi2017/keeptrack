<%@ page import="ch.zhaw.psit.keeptrack.model.User" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.UserDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.util.Credentials" %>
<%@ page import="java.util.Optional" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <title>KeepTrack! - Login</title>
    <script>
        $(function(){
        $("#footer").load("footer.html");
        });
    </script>
    <link rel="shortcut icon" href="../../img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon" />
    <link href="../../css/foundation.css" rel="stylesheet">
    <link href="../../css/app.css" rel="stylesheet" >
    <link href="../../css/header.css" rel="stylesheet">
</head>

<body>
  <div id="WhiteBackground" class="grid-container fluid">
    <div class="grid-x align-center">
      <header>
             <div class="container" >
             <div class="grid-x align-center">
                <div class="small-10 cell">
                    <h1><img src="../../img/keeptrack.png" alt="Logo as Titel" /></h1>
                </div>
             </div>
             <div class="large-6 cell" >
                 <h4 class="show-for-large">Ihr Partner für ein sichere Ausleihe</h4>
             </div>
         </div>
      </header>
    </div>
    <main>
        <div class="grid-x text-left" >
            <div class="small-3 cell small-offset-2">
                <h3>Login:</h3>
            </div>
        </div>

        <form data-abide novalidate method="post" action="/login">
            <div class="grid-x text-left" >
                    <div class="small-6 cell small-offset-3">
                      <div data-abide-error class="alert callout" style="display: none;">
                         <p><i class="fi-alert"></i> Die Eingaben sind nicht vollständig oder fehlerhaft.</p>
                      </div>
                      <div>
                        <label> Benutzername:
                         <input type="text" id="username" name="username" autofocus placeholder="Benutzername" aria-errormessage="errorUsername" required>
                            <span class="form-error" id="errorUsername">
                             Bitte Benutzername angeben.
                            </span>
                        </label>
                      </div>
                      <div>
                        <label>Passwort:
                            <input type="password" id="password" name="password" aria-errormessage="errorPassword" required>
                            <span class="form-error" id="errorPassword">
                                Bitte Passwort eingeben.
                            </span>
                        </label>
                      </div>
                </div>
            </div>
            <div class="grid-x text-center" >
                <div class="small-3 cell small-offset-2 medium-offset-3">
                    <button id="LoginButton" class="button" type="submit" value="Submit">Log-in</button>
                </div>
                <div class="small-3 cell">
                    <a href="register" class="button">Registrieren</a>
                </div>
            </div>
        </form>
        <br />
        <% if ("POST".equals(request.getMethod())) {

            Optional<User> optionalUser = new UserDAO().findByUsername(request.getParameter("username"));
            String password = Credentials.getInstance().hash(request.getParameter("password"));
            if (optionalUser.isPresent() && optionalUser.get().getPassword().equals(password)) {
                session.setAttribute("user", optionalUser.get());
                //setting session to expiry in 30 mins
                session.setMaxInactiveInterval(30 * 60);
                response.sendRedirect("dashboard");
            }
            else { %>
                <p style="font-size: medium; color: red; text-align: center;">Benutzername oder Passwort falsch</p>
                <br />
            <% }
        }

        %>

    </main>
  </div>

    <script src="../../js/vendor/jquery.js"></script>
    <script src="../../js/vendor/what-input.js"></script>
    <script src="../../js/vendor/foundation.js"></script>
    <script src="../../js/app.js"></script>
  </body>
</html>
