<%@ page import="ch.zhaw.psit.keeptrack.Loggers" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.UserDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.exception.UserExistsException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Date" %>
<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="user" class="ch.zhaw.psit.keeptrack.model.User"/>
<jsp:setProperty name="user" property="*"/>
<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Registration</title>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon"/>
    <link href="css/foundation.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <link href="css/registration.css" rel="stylesheet">
    <link href="css/header.css" rel="stylesheet"/>
</head>

<body>
<div id="WhiteBackground" class="grid-container">

    <% if (!"POST".equals(request.getMethod())) { %>
    <form action="register" method="post" data-abide novalidate>


        <div class="grid-x align-center">
            <div class="cell small-10 cell">
                <br/>
                <h2>Registrierung</h2>
                <h3>Nur noch ein paar Angaben und es kann losgehen</h3>
            </div>
        </div>
        <!-- Username -->
        <div class="grid-x align-center">
            <div class="cell small-10">
                <div data-abide-error class="alert callout" style="display: none;">
                    <p><i class="fi-alert"></i> Die Eingaben sind nicht vollständig oder fehlerhaft.</p>
                </div>

                <label>Benutzername
                    <input id="username" name="username" type="text" placeholder="Beispiel234"
                           aria-errormessage="errorUsername"
                           required pattern="[a-zA-Z0-9]+" maxlength="20">
                    <span class="form-error" id="errorUsername">
                        Ungültiger Benutzername.
                    </span>
                </label>
            </div>

            <div class="cell small-10">
                <label>Vorname
                    <input id="name" name="name" type="text" placeholder="Max"
                           aria-errormessage="errorName"
                           required pattern="[a-zA-Z ]+" maxlength="30">
                    <span class="form-error" id="errorName">
                    Darf nur Buchstaben enthalten.
                    </span>
                </label>
            </div>
        </div>


        <div class="grid-x align-center">
            <div class="cell small-10">
                <label>Nachname
                    <input id="surname" name="surname" type="text" placeholder="Müller"
                           aria-errormessage="errorSurname"
                           required pattern="[a-zA-Z ]+" maxlength="30">
                    <span class="form-error" id="errorSurname">
                    Darf nur Buchstaben enthalten.
                    </span>
                </label>
            </div>
        </div>

        <div class="grid-x align-center">
            <div class="cell small-10">
                <label>Adresse
                    <input id="address" name="address" type="text" placeholder="Bahnhofstrasse 13, Zürich"
                           aria-errormessage="errorAddress"
                           required maxlength="50">
                    <span class="form-error" id="errorAddress">
                    Ungültige Adresse.
                    </span>
                </label>
            </div>
        </div>

        <!-- Email -->
        <div class="grid-x align-center">
            <div class="cell small-10">
                <label>Email-Adresse
                    <input id="email" name="email" type="email" placeholder="max.muster@server.com"
                           aria-errormessage="errorEmail" required pattern="email">
                    <span class="form-error" id="errorEmail">
                    Ungültige Email-Adresse.
                    </span>
                </label>
            </div>
        </div>

        <!-- Passwort -->
        <div class="grid-x align-center">
            <div class="cell small-10">
                <label>Passwort
                    <input id="password" name="password" type="password" placeholder="********" required>
                    <span class="form-error" id="errorPassword">
                    Bitte geben Sie ein Passwort an.
                    </span>
                </label>
            </div>
        </div>
        <div class="grid-x align-center">
            <div class="cell small-10">
                <label>Re-enter Password
                    <input id="confirmPassword" type="password" placeholder="********" aria-errormessage="errorConfirmPassword" required
                           data-equalto="password">
                    <span class="form-error" id="errorConfirmPassword">
                    Das Passwort stimmt nicht überein.
                    </span>
                </label>
            </div>
        </div>


        <!--Buttons -->

        <div class="grid-x grid-margin-x align-center">
            <div class="cell small-2 text-center">
                <button id="RegisterButton" class="button" type="submit" value="Submit">Submit</button>
            </div>
        </div>


    </form>
    <br/>

    <% } else {

        Boolean userExists = false;
        try {
            user.setBirthday(Date.valueOf(LocalDate.now()));
            user = new UserDAO().register(user);
        } catch (UserExistsException e) {
            user = null;
            userExists = true;
        } catch (SQLException ex) {
            user = null;
            Loggers.DEVELOPMENT_LOGGER.warn("Unknown Error: ", ex);
        }

        if (user != null) {
    %>
    <span id="success">Registration Successful! You may log in.</span>
    <%
    } else if (userExists) {
    %>
    <span id="errorExistingUser">This user already exists. Please try again.</span>
    <%
    } else {
    %>
    <span id="errorUnknown">Unknown error.</span>
    <% }
    } %>

</div>


<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>
</body>
</html>
