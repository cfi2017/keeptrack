<%@ page import="ch.zhaw.psit.keeptrack.model.User" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Profil bearbeiten</title>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#footer").load("footer.html");
        });
    </script>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon" />
    <link href="css/foundation.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet" >
    <link href="css/header.css" rel="stylesheet"/>
    <link href="css/profile.css" rel="stylesheet"/>
</head>

<body>
<% if(session.getAttribute("user")!= null) { %>
<div class="grid-container fluid" id="WhiteBackground">
    <div class="grid-x align-center">
        <header class="cell small-10">
            <div class="grid-x align-center">
                <div class="container" >
                    <div class="grid-x align-center">
                        <div class="small-10 cell">
                            <h1><img src="../img/keeptrack.png" alt="Logo as Titel" /></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-x">
                <nav class="top-bar top-bar-responsive">
                    <div class="top-bar-title">
                    <span data-responsive-toggle="top-bar-responsive" data-hide-for="large">
                      <button class="menu-icon" type="button" data-toggle></button>
                    </span>
                    </div>
                    <div class="top-bar-responsive" id="top-bar-responsive">
                        <div class="top-bar-left">
                            <ul class="dropdown menu menu simple vertical large-horizontal" data-dropdown-menu>
                                <li class="menu-text"><a href="dashboard">KeepTrack!</a></li>
                                <li>
                                    <a href="search">Gegenstand suchen</a>
                                </li>
                                <li>
                                    <a href="add">Gegenstand erfassen</a>
                                </li>
                                <li>
                                    <a href="view">Eigene Gegenstände</a>
                                </li>
                                <li>
                                    <a href="profile">Profil</a>
                                    <ul class="menu vertical">
                                        <li><a href="logout">Log Out</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
    </div>
    <main class="cell small-10">
        <div class="grid-x text-center">
            <div class="cell expanded">
                <h2>Profil bearbeiten</h2>
            </div>
        </div>
        <%
            User user = (User) session.getAttribute("user");
        %>

        <div class="grid-x">
            <div class="cell small-3 small-offset-1">
                <button class="button" type="button" data-toggle="actions">Actions</button>
                <div class="dropdown-pane" id="actions" data-dropdown data-auto-focus="true">
                    <a class="button" id="new" href="/passwordReset"> Passwort ändern</a>
                    <button type="submit" class="button" id="edit">Daten anpassen</button>
                </div>
            </div>
        </div>
        <div class="grid-x align-center">
            <div class="cell small-12">
                <table>
                    <tr>
                        <th>Benutzername:</th>
                        <td><%=user.getUsername() %></td>
                        <td  class="rowspan" rowspan="5"></td>
                    </tr>
                    <tr>
                        <th>Vorname:</th>
                        <td><%=user.getName()%></td>
                    </tr>
                    <tr>
                        <th>Nachname:</th>
                        <td><%=user.getSurname() %></td>
                    </tr>
                    <tr>
                        <th>Geburtsdatum</th>
                        <td><%=user.getBirthday() %></td>
                    </tr>
                    <tr>
                        <th>Email-Adresse:</th>
                        <td><%=user.getEmail() %></td>
                    </tr>
                </table>
            </div>
        </div>
        <br />
    </main>
    <div id="footer"></div>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>

<% } else { response.sendRedirect("login"); } %>
</body>
</html>