<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Suche</title>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#footer").load("footer.html");
        });
    </script>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon" />
    <link href="../css/foundation.css" rel="stylesheet">
    <link href="../css/app.css" rel="stylesheet" >
    <link href="../css/search.css" rel="stylesheet" >
    <link href="../css/header.css" rel="stylesheet">
</head>

<body>
<% if(session.getAttribute("user")!= null) { %>
    <div class="grid-container fluid" id="WhiteBackground">
        <div class="grid-x align-center">
            <header class="cell small-10">
                <div class="grid-x align-center">
                    <div class="container" >
                        <div class="grid-x align-center">
                            <div class="small-10 cell">
                                <h1><img src="../img/keeptrack.png" alt="Logo as Titel" /></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-x">
                    <nav class="top-bar top-bar-responsive">
                        <div class="top-bar-title">
                    <span data-responsive-toggle="top-bar-responsive" data-hide-for="large">
                      <button class="menu-icon" type="button" data-toggle></button>
                    </span>
                        </div>
                        <div class="top-bar-responsive" id="top-bar-responsive">
                            <div class="top-bar-left">
                                <ul class="dropdown menu menu simple vertical large-horizontal" data-dropdown-menu>
                                    <li class="menu-text"><a href="dashboard">KeepTrack!</a></li>
                                    <li>
                                        <a href="search">Gegenstand suchen</a>
                                    </li>
                                    <li>
                                        <a href="add">Gegenstand erfassen</a>
                                    </li>
                                    <li>
                                        <a href="view">Eigene Gegenstände</a>
                                    </li>
                                    <li>
                                        <a href="profile">Profil</a>
                                        <ul class="menu vertical">
                                            <li><a href="logout">Log Out</a> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
        </div>
        <main class="cell small-10">
            <div class="grid-x text-center">
                <div class="cell expanded">
                    <h2>Gegenstand suchen</h2>
                </div>
            </div>

            <div class="grid-x align-center">
                <div class="cell small-12">
                    <div>
                        <form action="search.jsp" method="post" enctype="multipart/form-data">
                            Suchbegriff: <br/>
                            <input type="text" name="suchbegriff" placeholder="Suchbegriff"> <br/>
                            Filter setzen: <br/>
                            <label>
                                <select name="filter">
                                    <option value="kategorie">Kategorie auswählen</option>
                                    <option value="spielzeug">Spielzeug</option>
                                    <option value="buch">Buch</option>
                                    <option value="haushaltsgerät">Haushaltsgerät</option>
                                    <option value="gartengerät">Gartengerät</option>
                                    <option value="andere">Andere Kategorie</option>
                                </select>
                            </label> <br/>

                            Umkreis auswählen: <br/>
                            <label class="check">
                                <input type="checkbox" name="umkreis" value="1">
                            </label> 1km
                            <br />
                            <label class="check">
                                <input type="checkbox" name="umkreis" value="2">
                            </label> 2km
                            <br />
                            <label class="check">
                                <input type="checkbox" name="umkreis" value="5">
                            </label> 5km
                            <br />
                            <label class="check">
                                <input type="checkbox" name="umkreis" value="10">
                            </label> 10km
                            <br />
                            <label class="check">
                                <input type="checkbox" name="umkreis" value="1">
                            </label> >10km
                            <br />
                            <button class="button" type="submit" value="Submit">Suchen</button>
                        </form>
                    </div>
                    <br />
                    <div class="result">
                        <p class="results">Suchergebnisse:</p>
                        <hr width="95%">
                        <br />
                        <table>
                            <tr>
                                <th>Produktname</th>
                                <th>Hersteller</th>
                                <th>Kategorie</th>
                                <th>Zustand</th>
                                <th>Anbieter</th>
                                <th>Bewertung</th>
                                <th>Distanz</th>
                            </tr>
                            <tr>
                                <td>Rasenmäher</td>
                                <td>Bosch</td>
                                <td>Gartengerät</td>
                                <td>Gebraucht</td>
                                <td>Vanessa Baer</td>
                                <td>8/10</td>
                                <td>5km</td>
                            </tr>
                            <tr>
                                <td>Toaster</td>
                                <td>Philips</td>
                                <td>Küchengerät</td>
                                <td>Gebraucht</td>
                                <td>Anna-Lea Fuhrer</td>
                                <td>9/10</td>
                                <td>35km</td>
                            </tr>
                            <tr>
                                <td>Taschenrechner</td>
                                <td>Texas Instruments</td>
                                <td>Andere Kategorie</td>
                                <td>Praktisch neu</td>
                                <td>Philip Keller</td>
                                <td>4/10</td>
                                <td>3km</td>
                            </tr>
                            <tr>
                                <td>Bohrmaschine</td>
                                <td>Bosch</td>
                                <td>Werkzeug</td>
                                <td>Gebraucht</td>
                                <td>Anna-Lea Fuhrer</td>
                                <td>9/10</td>
                                <td>35km</td>
                            </tr>
                            <tr>
                                <td>Hammer</td>
                                <td>Lux</td>
                                <td>Werkzeug</td>
                                <td>Gebraucht</td>
                                <td>Anna-Lea Fuhrer</td>
                                <td>9/10</td>
                                <td>35km</td>
                            </tr>
                            <tr>
                                <td>Schraubenzieher</td>
                                <td>PB Swiss Tools</td>
                                <td>Werkzeug</td>
                                <td>Neu</td>
                                <td>Vanessa Baer</td>
                                <td>8/10</td>
                                <td>5km</td>
                            </tr>
                            <tr>
                                <td>Staubsauger</td>
                                <td>Dyson</td>
                                <td>Haushaltsgerät</td>
                                <td>Ungebraucht</td>
                                <td>Max Muster</td>
                                <td>10/10</td>
                                <td>80km</td>
                            </tr>
                            <tr>
                                <td>Mixer</td>
                                <td>Siemens</td>
                                <td>Küchengerät</td>
                                <td>Gebraucht</td>
                                <td>Philip Keller</td>
                                <td>4/10</td>
                                <td>3km</td>
                            </tr>
                            <tr>
                                <td>Audioanlage</td>
                                <td>Sony</td>
                                <td>Multimediagerät</td>
                                <td>Neu</td>
                                <td>Philip Keller</td>
                                <td>4/10</td>
                                <td>3km</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <div id="footer"></div>
    </div>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.js"></script>
    <script src="../js/app.js"></script>

<% } else { response.sendRedirect("login"); } %>
</body>
</html>