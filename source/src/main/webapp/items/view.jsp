<%@ page import="java.util.List" %>
<%@ page import="ch.zhaw.psit.keeptrack.model.Item" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.ItemDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.model.User" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.UserDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.CategoryDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.model.Category" %>
<%@ page import="java.util.Optional" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Eigene Objekte</title>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#footer").load("footer.html");
        });
    </script>
    <script src="/js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon" />
    <link href="../css/foundation.css" rel="stylesheet">
    <link href="../css/app.css" rel="stylesheet" >
    <link href="../css/header.css" rel="stylesheet"/>
    <link href="../css/viewObjects.css" rel="stylesheet"/>
</head>

<body>
<% if(session.getAttribute("user")!= null) { %>
    <div class="grid-container fluid" id="WhiteBackground">
        <div class="grid-x align-center">
            <header class="cell small-10">
                <div class="grid-x align-center">
                    <div class="container" >
                        <div class="grid-x align-center">
                            <div class="small-10 cell">
                                <h1><img src="../img/keeptrack.png" alt="Logo as Titel" /></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-x">
                    <nav class="top-bar top-bar-responsive">
                        <div class="top-bar-title">
                    <span data-responsive-toggle="top-bar-responsive" data-hide-for="large">
                      <button class="menu-icon" type="button" data-toggle></button>
                    </span>
                        </div>
                        <div class="top-bar-responsive" id="top-bar-responsive">
                            <div class="top-bar-left">
                                <ul class="dropdown menu menu simple vertical large-horizontal" data-dropdown-menu>
                                    <li class="menu-text"><a href="dashboard">KeepTrack!</a></li>
                                    <li>
                                        <a href="search">Gegenstand suchen</a>
                                    </li>
                                    <li>
                                        <a href="add">Gegenstand erfassen</a>
                                    </li>
                                    <li>
                                        <a href="view">Eigene Gegenstände</a>
                                    </li>
                                    <li>
                                        <a href="profile">Profil</a>
                                        <ul class="menu vertical">
                                            <li><a href="logout">Log Out</a> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
        </div>
        <main class="cell small-10">
            <div class="grid-x text-center">
                <div class="cell expanded">
                    <h2>Meine Objekte</h2>
                </div>
            </div>
            <div class="grid-x align-center">
                <div class="cell small-12">
                    <div class="result">
                        <table>
                            <tr>
                                <th>Produktname</th>
                                <th>Hersteller</th>
                                <th>Kategorie</th>
                                <th>Status</th>
                                <th>Aktueller Besitzer</th>
                                <th>Datum der Rückgabe</th>
                                <th></th>
                            </tr>
                            <%
                                int indexButton = 0;
                                User user = (User) session.getAttribute("user");
                                List<Item> items = new ItemDAO().getOwnObjects(user.getId());
                                for (Item item : items) {
                            %>
                            <tr>
                                <td><%=item.getName()%></td>
                                <td><%=item.getManufacturer()%></td>
                                <td>
                                    <%
                                    Optional<Category> optionalCategory = new CategoryDAO().retrieve(item.getCategory());
                                    if (optionalCategory.isPresent()) {
                                    %>
                                    <%=optionalCategory.get().getName()%>
                                    <%
                                        }
                                    %>
                                </td>
                                <td>
                                <%
                                if(item.getPossessor() == item.getOwner()) { %>
                                    Verfügbar
                                <% }
                                else { %>
                                   Ausgeliehen
                                <% }
                                %></td>
                                <td>
                                    <%
                                        Optional<User> optionalUser = new UserDAO().retrieve(item.getPossessor());
                                        if (optionalUser.isPresent()) {
                                    %>
                                    <%=optionalUser.get().getUsername()%>
                                    <%
                                        }
                                    %>
                                </td>
                                <td>Offen</td>
                                <td>
                                    <div class="grid-x">
                                    <div class="cell small-3">
                                        <button class="button" type="button" data-toggle="actions<%=indexButton%>">Actions</button>
                                        <div class="dropdown-pane" id="actions<%=indexButton%>" data-dropdown data-auto-focus="true">
                                            <a class="button" id="new" href="/share">Freigeben</a>
                                            <a class="button" id="edit" href="/edit">Bearbeiten</a>
                                            <a class="button" id="delete" href="/delete">Löschen</a>
                                        </div>
                                    </div>
                                </div></td>
                            </tr>
                            <%
                                indexButton++;
                                }
                            %>
                        </table>
                    </div>
                </div>
            </div>
            <br />
        </main>
        <div id="footer"></div>
    </div>

<script src="../js/vendor/jquery.js"></script>
<script src="/js/vendor/fastclick.js"></script>
<script src="../js/vendor/what-input.js"></script>
<script src="../js/vendor/foundation.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="../js/app.js"></script>

<% } else { response.sendRedirect("login"); } %>
</body>
</html>