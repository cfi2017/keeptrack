<%@ page import="ch.zhaw.psit.keeptrack.model.Item" %>
<%@ page import="ch.zhaw.psit.keeptrack.model.Category" %>
<%@ page import="java.util.List" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.CategoryDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.dao.ItemDAO" %>
<%@ page import="ch.zhaw.psit.keeptrack.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("login");
    }

    Boolean isError = false;
    if ("POST".equals(request.getMethod())) {
        Item item = new Item();
        item.setName(request.getParameter("name"));
        item.setDescription(request.getParameter("beschreibung"));
        item.setManufacturer(request.getParameter("hersteller"));
        item.setCondition(request.getParameter("zustand"));
        int result = Integer.parseInt(request.getParameter("kategorie"));
        item.setCategory(result);
        item.setOwner(((User) session.getAttribute("user")).getId());
        item.setPossessor(((User) session.getAttribute("user")).getId());
        ItemDAO itemDAO = new ItemDAO();
        itemDAO.create(item);
        if (item != null) {
            response.sendRedirect("view");
        } else {
            isError = true;
        }
    }
%><!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KeepTrack! - Objekt erfassen</title>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#footer").load("footer.html");
        });
    </script>

    <link rel="shortcut icon" href="img/KeepTrack_Logo.PNG" type="image/vnd.microsoft.icon"/>
    <link href="../css/foundation.css" rel="stylesheet">
    <link href="../css/app.css" rel="stylesheet">
    <link href="../css/header.css" rel="stylesheet"/>
    <link href="../css/addObject.css" rel="stylesheet"/>
</head>

<body>

<div class="grid-container fluid" id="WhiteBackground">
    <div class="grid-x align-center">
        <header class="cell small-10">
            <div class="grid-x align-center">
                <div class="container">
                    <div class="grid-x align-center">
                        <div class="small-10 cell">
                            <h1><img src="../img/keeptrack.png" alt="Logo as Titel"/></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-x">
                <nav class="top-bar top-bar-responsive">
                    <div class="top-bar-title">
                    <span data-responsive-toggle="top-bar-responsive" data-hide-for="large">
                      <button class="menu-icon" type="button" data-toggle></button>
                    </span>
                    </div>
                    <div class="top-bar-responsive" id="top-bar-responsive">
                        <div class="top-bar-left">
                            <ul class="dropdown menu menu simple vertical large-horizontal" data-dropdown-menu>
                                <li class="menu-text"><a href="dashboard">KeepTrack!</a></li>
                                <li>
                                    <a href="search">Gegenstand suchen</a>
                                </li>
                                <li>
                                    <a href="add">Gegenstand erfassen</a>
                                </li>
                                <li>
                                    <a href="view">Eigene Gegenstände</a>
                                </li>
                                <li>
                                    <a href="profile">Profil</a>
                                    <ul class="menu vertical">
                                        <li><a href="logout">Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
    </div>

    <main class="cell small-10">
        <% if(!isError) { %>
            <div class="grid-x text-center">
                <div class="cell expanded">
                    <h2>Gegenstand erfassen</h2>
                </div>
            </div>

            <div class="grid-x align-center">
                <div class="cell small-12">
                    <form data-abide novalidate method="post" action="/add">
                        Name*: <br/>
                        <input type="text" id="name" name="name" required="true" placeholder="Name"> <br/>
                        Hersteller*: <br/>
                        <input type="text" id="hersteller" name="hersteller" required="true" placeholder="Hersteller"> <br/>
                        Zustand*: <br/>
                        <input type="text" id="zustand" name="zustand" required="true" placeholder="Zustand"> <br/>
                        Kategorie*: <br/>
                        <label>
                            <select id="kategorie" name="kategorie" required="true">
                                <option value="">Kategorie wählen</option>
                                <%
                                    List<Category> categories = new CategoryDAO().getAll();
                                    for (Category category : categories) {
                                %>
                                <option value="<%=category.getId()%>"><%=category.getName()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </label> <br/>
                        Detaillierte Beschreibung des Gegenstandes: <br/>
                        <textarea id="beschreibung" name="beschreibung" rows="8" cols="60"
                                  placeholder="Beschreibung Gegenstand"></textarea> <br/>
                        <!--<input name="bild" type="file" size="20" accept="image/*"> <br/>-->
                        <button class="button" type="submit" value="Submit">Erfassen</button>
                    </form>
                </div>
            </div>
        <% } else {%>
            <div class="grid-x text-center">
                <div class="cell expanded">
                    <h2>Unknown error.</h2>
                </div>
            </div>
        <% } %>
    </main>
    <div id="footer"></div>
</div>
<script src="../js/vendor/jquery.js"></script>
<script src="../js/vendor/what-input.js"></script>
<script src="../js/vendor/foundation.js"></script>
<script src="../js/app.js"></script>
</body>
</html>