package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.exception.UserExistsException;
import ch.zhaw.psit.keeptrack.model.User;
import ch.zhaw.psit.keeptrack.util.Credentials;

import java.sql.PreparedStatement;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserDAO extends DataAccessObject<User> {

    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with either the insert statement or the statement to find the new id
     */
    @Override
    public User create(User entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("INSERT INTO users (id, username, password, name, surname, birthday, email, address) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");
        fillStatement(entity, statement);
        statement.execute();

        int id = this.getLastSetId();
        entity.setId(id);
        return entity;
    }

    private void fillStatement(User entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getUsername());
        statement.setString(2, entity.getPassword());
        statement.setString(3, entity.getName());
        statement.setString(4, entity.getSurname());
        statement.setDate(5, entity.getBirthday());
        statement.setString(6, entity.getEmail());
        statement.setString(7, entity.getAddress());
    }

    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with the update statement
     */
    @Override
    public User update(User entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "UPDATE USERS SET username = ?, password = ?, name = ?, surname = ?, birthday = ?, email = ?, address = ? WHERE id = ?");
        fillStatement(entity, statement);
        statement.setInt(8, entity.getId());
        statement.execute();

        return entity;
    }

    /**
     * @param entity entity to persist
     * @throws SQLException if there is an error with the delete statement
     */
    @Override
    public void delete(User entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("DELETE FROM USERS WHERE id = ?");
        statement.setInt(1, entity.getId());
        statement.execute();
    }

    /**
     * @param id
     * @return User or null if user is not set.
     */
    @Override
    public Optional<User> retrieve(Integer id) throws SQLException {

        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM USERS WHERE id = ?");
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        return resultsToEntity(resultSet, User.class);
    }

    public Optional<User> findByEmail(String email) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM USERS WHERE email = ?");
        statement.setString(1, email);
        ResultSet resultSet = statement.executeQuery();

        return resultsToEntity(resultSet, User.class);
    }

    public Optional<User> findByUsername(String username) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM USERS WHERE username = ?");
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        return resultsToEntity(resultSet, User.class);

    }

    public User register(User user) throws UserExistsException, SQLException {
        // todo: validation
        if (!this.findByEmail(user.getEmail()).isPresent() && !this.findByUsername(user.getUsername()).isPresent()) {
            Loggers.DEVELOPMENT_LOGGER.info("Creating user with email <{}>", user.getEmail());
            user.setPassword(Credentials.getInstance().hash(user.getPassword()));
            return this.create(user);
        } else throw new UserExistsException(user);

    }
}
