package ch.zhaw.psit.keeptrack.conf;

class ConfigurationException extends Exception {

    ConfigurationException(String message) {
        super(message);
    }

}
