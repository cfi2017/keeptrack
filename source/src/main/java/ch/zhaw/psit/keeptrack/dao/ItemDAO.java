package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.exception.UserExistsException;
import ch.zhaw.psit.keeptrack.model.Item;
import ch.zhaw.psit.keeptrack.model.User;
import ch.zhaw.psit.keeptrack.util.Credentials;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ItemDAO extends DataAccessObject<Item>  {


    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with either the insert statement or the statement to find the new id
     */
    @Override
    public Item create(Item entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("INSERT INTO ITEMS " +
                "(id, name, description, manufacturer, `condition`, category, owner, possessor) " +
                        "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");
        fillStatement(entity, statement);
        statement.execute();

        int id = this.getLastSetId();
        entity.setId(id);
        return entity;
    }

    protected void fillStatement(Item entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getDescription());
        statement.setString(3, entity.getManufacturer());
        statement.setString(4, entity.getCondition());
        statement.setInt(5, entity.getCategory());
        statement.setInt(6, entity.getOwner());
        statement.setInt(7, entity.getPossessor());
    }

    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with the update statement
     */
    @Override
    public Item update(Item entity)throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("UPDATE ITEMS SET name = ?, description = ?, manufacturer = ?, `condition` = ?, category = ?, owner = ?, possessor = ? WHERE id = ?");
        statement.setInt(8, entity.getId());
        fillStatement(entity, statement);
        statement.execute();

        return entity;
    }

    /**
     * @param entity entity to persist
     * @throws SQLException if there is an error with the delete statement
     */
    @Override
    public void delete(Item entity)throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("DELETE FROM ITEMS WHERE id = ?");
        statement.setInt(1, entity.getId());
        statement.execute();
    }

    /**
     *
     * @param id
     * @return null
     */
    @Override
    public Optional<Item> retrieve(Integer id) throws SQLException {

        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM ITEMS WHERE id = ?");
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        return resultsToEntity(resultSet, Item.class);
    }

    public List<Item> getOwnObjects(int userId) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM ITEMS WHERE owner = ?");
        statement.setInt(1, userId);
        ResultSet resultSet = statement.executeQuery();
        return resultsToList(resultSet, Item.class);
    }


}
