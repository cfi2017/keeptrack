package ch.zhaw.psit.keeptrack.conf;

import ch.zhaw.psit.keeptrack.Loggers;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ConfigLoader {

    private static ConfigLoader instance;
    private JSONObject config;

    public static ConfigLoader getInstance() {
        if (instance == null) {
            instance = new ConfigLoader();
            String source = System.getProperty("KT_CONF_SOURCE");
            if (source == null) source = System.getenv("KT_CONF_SOURCE");
            if (source == null) source = "test.config.json";
            Loggers.DEVELOPMENT_LOGGER.info("Config source: <{}>", source);
            try {
                instance.setSource(source);
            } catch (ConfigurationException ex) {
                Loggers.DEVELOPMENT_LOGGER.warn("Specified configuration not found or no configuration provided: ", ex);
                throw new RuntimeException("Specified configuration not found or no configuration provided.");
            }
        }
        return instance;
    }

    /**
     * @param source configuration source to set
     * @throws ConfigurationException if no valid source is provided or source file is malformed
     */
    public void setSource(String source) throws ConfigurationException {
        if (source == null) return;
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(source);
        if (inputStream == null) throw new ConfigurationException("Configuration not found: " + source);
        try {
            this.config = readJSONStream(inputStream);
        } catch (IOException | JSONException ex) {
            throw new ConfigurationException("Error reading config file: " + source);
        }
    }

    /**
     * returns the configuration value for the given key. checks system properties, then environment variables,
     * then the config file.
     * @param key -
     * @return the configuration for the given key.
     */
    // todo: check possible usage of Optional instead of @Nullable
    public String get(String key) {
        if (key == null) return null;

        // todo: check system variables
        if (System.getProperty(key) != null) {
            return System.getProperty(key);
        }

        // todo: check environment variables
        if (System.getenv(key) != null) {
            return System.getenv(key);
        }
        if (this.config == null) return null;
        try {
            JSONObject json = this.config;

            return json.getString(key);
        } catch (JSONException e) {
            return null;
        }
    }

    private static JSONObject readJSONStream(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        return new JSONObject(new String(buffer.toByteArray()));
    }

}
