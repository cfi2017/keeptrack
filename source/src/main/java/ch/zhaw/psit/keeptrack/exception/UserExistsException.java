package ch.zhaw.psit.keeptrack.exception;

import ch.zhaw.psit.keeptrack.model.User;

public class UserExistsException extends Exception {
    private final User user;

    public UserExistsException(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
