package ch.zhaw.psit.keeptrack.model;

import ch.zhaw.psit.keeptrack.dao.Column;
import ch.zhaw.psit.keeptrack.dao.Entity;

public class Category implements Entity {

    @Column("id")
    private Integer id;

    @Column("name")
    private String name;

    public Category() {
        
    }

    /**
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id set id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name set name
     */
    public void setName(String name) {
        this.name = name;
    }
}
