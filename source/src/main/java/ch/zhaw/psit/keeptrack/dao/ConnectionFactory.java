package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import ch.zhaw.psit.keeptrack.conf.ConfigLoader;
import com.mysql.jdbc.NotImplemented;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static ConnectionFactory instance;

    public static ConnectionFactory getInstance() {
        if (instance == null) {
            instance = new ConnectionFactory();
        }
        return instance;
    }

    public Connection createConnection() throws SQLException {
        String connectionString = ConfigLoader.getInstance().get("DB_STRING");
        String user = ConfigLoader.getInstance().get("DB_USER");
        String pass = ConfigLoader.getInstance().get("DB_PASS");
        String authMode = ConfigLoader.getInstance().get("DB_AUTH");

        if (connectionString == null) {
            Loggers.DEVELOPMENT_LOGGER.error("No Database Connection String found. " +
                    "Did you specify a configuration file? " +
                    "You can specify an environment variable or system property called DB_STRING " +
                    "or add the attribute DB_STRING to a config json that you load with either " +
                    "the environment variable KT_CONF_SOURCE or a system property with the same name");
            throw new RuntimeException("No database connection string found.");
        }

        try {
            if (connectionString.startsWith("jdbc:h2:")) {
                Class.forName("org.h2.Driver");
            } else if (connectionString.startsWith("jdbc:mysql:")) {
                Class.forName("com.mysql.jdbc.Driver");
            } else {
                // no driver for specified URL
                throw new NotImplementedException();
            }
        } catch (ClassNotFoundException ex) {
            Loggers.DEVELOPMENT_LOGGER.info("Exception trying to load the driver: ", ex);
            throw new RuntimeException("Exception trying to load the driver: ", ex);
        }

        if (authMode == null || authMode.equals("none")) {
            return DriverManager.getConnection(connectionString);
        } else return DriverManager.getConnection(connectionString, user, pass);

    }

}
