package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.model.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class CategoryDAO extends DataAccessObject<Category> {

    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with either the insert statement or the statement to find the new id
     */
    @Override
    public Category create(Category entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("INSERT INTO CATEGORIES VALUES (NULL, ?)");
        statement.setString(1, entity.getName());
        statement.execute();

        int id = this.getLastSetId();
        entity.setId(id);
        return entity;
    }

    /**
     * @param entity entity to persist
     * @return entity: same instance as passed, but with an id
     * @throws SQLException if there is an error with the update statement
     */
    @Override
    public Category update(Category entity)throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("UPDATE CATEGORIES SET name = ? WHERE id = ?");
        statement.setString(1, entity.getName());
        statement.setInt(2, entity.getId());
        statement.execute();

        return entity;
    }

    /**
     * @param entity entity to persist
     * @throws SQLException if there is an error with the delete statement
     */
    @Override
    public void delete(Category entity) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("DELETE FROM CATEGORIES WHERE id = ?");
        statement.setInt(1, entity.getId());
        statement.execute();
    }

    /**
     *
     * @return resultsToList of categories
     * @throws SQLException if there is an error with the select statement
     */
    public List<Category> getAll() throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM categories");
        ResultSet resultSet = statement.executeQuery();
        return resultsToList(resultSet, Category.class);
    }

    /**
     *
     * @param id of the entity to retrieve
     * @return Optional of Category depending on if the category exists
     * @throws SQLException if the id is invalid
     */
    @Override
    public Optional<Category> retrieve(Integer id) throws SQLException {

        PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM CATEGORIES WHERE id = ?");
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();

        return resultsToEntity(resultSet, Category.class);
    }
}
