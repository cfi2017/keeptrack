package ch.zhaw.psit.keeptrack.dao;

import ch.zhaw.psit.keeptrack.Loggers;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.*;

public abstract class DataAccessObject<T extends Entity> {

    private static ThreadLocal<Connection> connection = new ThreadLocal<>();

    // todo: close connection on request end
    protected static Connection getConnection() {
        try {
            if (connection.get() == null) {
                connection.set(ConnectionFactory.getInstance().createConnection());
            }
            if (connection.get().isClosed()) {
                connection.set(ConnectionFactory.getInstance().createConnection());
            }
        } catch (SQLException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error trying to open database connection.", ex);
        }
        return connection.get();
    }

    /**
     * @return the last id that was created in the database
     * @throws SQLException
     */
    protected int getLastSetId() throws SQLException {
        Statement idStatement = getConnection().createStatement();
        idStatement.execute("SELECT LAST_INSERT_ID()");
        ResultSet set = idStatement.getResultSet();
        set.first();
        return idStatement.getResultSet().getInt(1);
    }

    List<T> resultsToList(ResultSet resultSet, Class<T> clazz) throws SQLException {
        List<T> results = new ArrayList<>();
        try {

            if (!resultSet.next()) {
                return results;
            } else do {
                T entity = clazz.newInstance();

                Arrays.stream(clazz.getDeclaredFields())
                        .filter(field -> field.isAnnotationPresent(Column.class))
                        .forEach(field -> {
                            String column = field.getAnnotation(Column.class).value();
                            try {
                                Object object = resultSet.getObject(column);
                                if (!field.isAccessible()) {
                                    field.setAccessible(true);
                                }
                                field.set(entity, object);
                            } catch (IllegalAccessException | SQLException e) {
                                Loggers.DEVELOPMENT_LOGGER.warn("Error: ", e);
                            }
                        });

                results.add(entity);
            } while (resultSet.next());

        } catch (IllegalAccessException | InstantiationException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Invalid class to parse: <{}>", clazz.getName(), ex);
        }

        return results;
    }

    Optional<T> resultsToEntity(ResultSet resultSet, Class<T> clazz) throws SQLException {
        List<T> res = resultsToList(resultSet, clazz);
        if (res.size() == 1) {
            return Optional.of(res.get(0));
        } else if (res.size() == 0) {
            return Optional.empty();
        } else {
            throw new NotImplementedException();
        }
    }

    public abstract T create(T entity) throws SQLException;

    public abstract T update(T entity) throws SQLException;

    public abstract void delete(T entity) throws SQLException;

    public abstract Optional<T> retrieve(Integer id) throws SQLException;

}
