package ch.zhaw.psit.keeptrack.model;

import ch.zhaw.psit.keeptrack.dao.Column;
import ch.zhaw.psit.keeptrack.dao.Entity;
import ch.zhaw.psit.keeptrack.dao.UserDAO;
import ch.zhaw.psit.keeptrack.util.Credentials;

import java.sql.Date;
import java.sql.SQLException;


public class User implements Entity {

    @Column("id")
    private Integer id;
    @Column("username")
    private String username;
    @Column("password")
    private String password;
    @Column("name")
    private String name;
    @Column("surname")
    private String surname;
    @Column("birthday")
    private Date birthday;
    @Column("email")
    private String email;
    @Column("address")
    private String address;

public User() {}

    /**
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id set id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username set username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password set password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name set name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname set surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     *
     * @param birthday set birthday
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email set email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address set address
     */
    public void setAddress(String address) {
        this.address = address;
    }
}

