package ch.zhaw.psit.keeptrack.util;

import ch.zhaw.psit.keeptrack.Loggers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Credentials {

    private static Credentials instance;

    public static Credentials getInstance() {
        if (instance == null) {
            instance = new Credentials("SHA-256");
        }
        return instance;
    }

    private String algorithm;

    public Credentials(String algo) {
        this.algorithm = algo;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * This returns a utf-8 encoded 64-character string representing the sha-256 hash of the given password.
     *
     * @param password password to hash
     * @return the string representation of the sha-256 hash of the given string
     */
    public String hash(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] bytes = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(bytes);

        } catch (NoSuchAlgorithmException ex) {
            Loggers.DEVELOPMENT_LOGGER.error("Error: Cannot find algorithm <{}>", this.algorithm);
            throw new RuntimeException("Fatal Error: Password Hashing Algorithm not found.");
        }
    }

    private String bytesToHex(byte[] hash) {
        StringBuilder res = new StringBuilder();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) res.append('0');
            res.append(hex);
        }
        return res.toString();
    }

}
