package ch.zhaw.psit.keeptrack.model;

import ch.zhaw.psit.keeptrack.dao.Column;
import ch.zhaw.psit.keeptrack.dao.Entity;

public class Item implements Entity {

    @Column("id")
    private Integer id;
    @Column("name")
    private String name;
    @Column("description")
    private String description;
    @Column("manufacturer")
    private String manufacturer;
    @Column("condition")
    private String condition;
    @Column("category")
    private int category;
    @Column("owner")
    private int owner;
    @Column("possessor")
    private int possessor;

    public Item() {


    }

    /**
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id set id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name set name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description set description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     *
     * @param manufacturer set manufacturer
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     *
     * @return condition
     */
    public String getCondition() {
        return condition;
    }

    /**
     *
     * @param condition set condition
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     *
     * @return category
     */
    public int getCategory() {
        return category;
    }

    /**
     *
     * @param category set category
     */
    public void setCategory(int category) {
        this.category = category;
    }

    /**
     *
     * @return owner
     */
    public int getOwner() {
        return owner;
    }

    /**
     *
     * @param owner set owner
     */
    public void setOwner(int owner) {
        this.owner = owner;
    }

    /**
     *
     * @return possessor
     */
    public int getPossessor() {
        return possessor;
    }

    /**
     *
     * @param prossessor set prossessor
     */
    public void setPossessor(int prossessor) {
        this.possessor = prossessor;
    }
}
