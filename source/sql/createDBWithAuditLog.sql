DROP DATABASE IF EXISTS keeptrack;
CREATE DATABASE keeptrack;
USE keeptrack;

-- necessary tables:
# users

CREATE TABLE USERS (
  id            INT AUTO_INCREMENT NOT NULL,
  username      VARCHAR(20) NOT NULL,
  password      VARCHAR(64) NOT NULL,

  name          VARCHAR(30) NOT NULL,
  surname       VARCHAR(30) NOT NULL,
  email         VARCHAR(50) NOT NULL,
  address       VARCHAR(50) NOT NULL,

  CONSTRAINT pk_users PRIMARY KEY (id),
  CONSTRAINT uk_users_email UNIQUE (email)
);

# category
CREATE TABLE CATEGORIES (
  id            INT AUTO_INCREMENT NOT NULL,
  name          VARCHAR(30) NOT NULL,

  CONSTRAINT pk_categories PRIMARY KEY (id)
);

# items
CREATE TABLE ITEMS (
  id            INT AUTO_INCREMENT NOT NULL,
  name          VARCHAR(30) NOT NULL,
  description   VARCHAR(200) NOT NULL,
  manufacturer  VARCHAR(50) NULL,
  `condition`   VARCHAR(30) NOT NULL,
  category      INT NOT NULL,
  owner         INT NOT NULL,
  possessor     INT NULL,


  CONSTRAINT pk_items PRIMARY KEY (id),
  CONSTRAINT fk_items_category FOREIGN KEY (category) REFERENCES CATEGORIES(id),
  CONSTRAINT fk_items_owner FOREIGN KEY (owner) REFERENCES USERS(id),
  CONSTRAINT fk_items_possessor FOREIGN KEY (possessor) REFERENCES USERS(id)
);

# user_roles
CREATE TABLE USER_ROLES (
  id            INT AUTO_INCREMENT NOT NULL,
  user          INT NOT NULL,
  role          VARCHAR(20) NOT NULL,

  CONSTRAINT pk_user_roles PRIMARY KEY (id),
  CONSTRAINT fk_user_roles_users FOREIGN KEY (user) REFERENCES USERS(id)
);

-- Auditing

CREATE TABLE AUDIT_LOG (
  id            INT AUTO_INCREMENT NOT NULL,
  fromUser      INT NOT NULL,
  toUser        INT NOT NULL,
  action        varchar(30),
  item          INT NOT NULL,

  CONSTRAINT pk_audit_log PRIMARY KEY (id),
  CONSTRAINT fk_audit_log_from_user FOREIGN KEY (fromUser) REFERENCES USERS(id),
  CONSTRAINT fk_audit_log_to_user FOREIGN KEY (toUser) REFERENCES USERS(id),
  CONSTRAINT fk_audit_log_item FOREIGN KEY (item) REFERENCES ITEMS(id)
);